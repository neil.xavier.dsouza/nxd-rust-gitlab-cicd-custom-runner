FROM docker:23.0-dind
RUN apk update
RUN apk add curl starship starship-zsh-completion starship-zsh-plugin vim helix ripgrep ripgrep-doc ripgrep-zsh-completion zsh
RUN apk add git g++ gcc clang
RUN cd /root
RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs > rustup.sh
RUN sh ./rustup.sh -y  --profile complete
RUN source "$HOME/.cargo/env" 
#CMD ["sh", "-c", "PATH=$PATH:/root/.cargo/bin sh"]
#https://stackoverflow.com/questions/49676490/when-installing-rust-toolchain-in-docker-bash-source-command-doesnt-work
ENV PATH="/root/.cargo/bin:${PATH}"